<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="estilos.css" type="text/css">
    <title>Ejercicio 3</title>
</head>
<body>
    <?php
        $N = 30;
        echo "<table>";
            for($i = 0; $i <= $N; $i++) {
                if($i%2==0) {
                    echo "<tr>";
                    echo "  <td class = 'par'>" . "$i" . "</td>";
                    echo "</tr>"; 
                } 
            };
        echo "</table";
    ?>
</body>
</html>