<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7</title>
</head>
<body>
    <?php
        function palindromo($string){
            if (strrev($string) == $string){
                return true;
            }
            else{
                return false;
            }
        }

        $palabra = "radar";
        if(palindromo($palabra)){
            echo "La palabra es un palindromo";
        }
        else {
        echo "La palabra no es un palindromo";
        }
        ?>
</body>
</html>