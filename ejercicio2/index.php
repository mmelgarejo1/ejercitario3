<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>
<body>
    <?php
        echo "La version de php es: " .phpversion(). "<br>";
        echo "El id de la version es: " . PHP_VERSION_ID. "<br>";
        echo "El valor maximo de enteros soportado para la version es: " . PHP_INT_MAX. "<br>";
        echo "Version del sistema operativo: " . php_uname(). "<br>";
        echo "El simbolo correcto para el fin de linea es: " . PHP_EOL . "<br>";
        echo "El include path por defecto es: " .get_include_path() . "<br>";
    ?>
</body>
</html>