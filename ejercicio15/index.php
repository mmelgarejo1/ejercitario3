<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 15</title>
</head>
<body>
    <?php
        $vector = new SplFixedArray(20);
        for($i=0;$i<sizeof($vector);$i++) {
            $vector[$i] = rand(1,10);
            echo "[" .$vector[$i] . "]";
        }
        echo "<br><br>";
        function imprimir($array) {
            for($i=sizeof($array)-1;$i>=0;$i--) {
                echo "[" .$array[$i] . "]";
            }
        }
        imprimir($vector);
    ?>
</body>
</html>