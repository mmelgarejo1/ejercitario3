<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 9</title>
</head>
<body>
    <?php
        $vector = new SplFixedArray(100);
        $sum = 0;
        for($i=0;$i<sizeof($vector);$i++) {
            $vector[$i] = rand(1,100);
            $sum = $sum + $vector[$i];
            echo "[" .$vector[$i]. "]";
        }
        echo "<br><br>La suma de los elementos del vector es = " .$sum;
    ?>
</body>
</html>